/* Copyright 2012 Red Giant Software. All rights reserved. */
// Plugin Definition
ui.plugin(
		// Fx Name
		"Matte Process",
		// Category
		"Primatte",
		// Company Name
		"Red Giant",
		// Company Website
		"http://www.redgiant.com/products/all/keying-suite",
		// Plugin Description
		"Full featured Keyer Matte processing",
		// Major Version
		"1",
		// Minor Version
		"0",
		// Bug Version
		"0",
		// UUID Fx
		"758AAC17-A5FE-483B-9592-74C897BAE9C0",
		// UUID Category
		"01495B47-8338-4679-95B9-64C6BE7026D0"
);

ui.importModules("bx");

ui.license("discrete");

ui.add_cpp_code("\
#define RG_PRODUCT_LIC_NAME RGS::kPrimatteKeyer\n\
#define SUITE_1_SERIAL_LIB\n\
#define SUITE_1_PRODUCT RGS::kProKeyingSuite\n\
#define SUITE_1_MAJOR 11\n\
#define SUITE_1_MINOR 0\n\
#define SUITE_1_BUG 0\n\
");


// Properties
var invert = ui.checkbox("Invert", false);
var preRadius = ui.slider("Pre Blur", 5.0, 0.0, 100.0);
var in_black = ui.slider("Input Black", 0, -1000.0, 100.0);
var in_white = ui.slider("Input White", 100, 0.0, 1000.0);
var gamma = ui.slider("Gamma", 1.0, 0.01, 16.000000);
var out_black = ui.slider("Output Black", 0, 0.0, 100.0);
var out_white = ui.slider("Output White", 100, 0.0, 100.0);

var blendMode = ui.blendMenu("Blend Mode",  bx.NONE);
var postRadius = ui.slider("Post Blur", 0.0, 0.0, 100.0);
var postErode = ui.slider("Post Erode", 0.0, 0.0, 100.0);
var median = ui.checkbox("Post Median", false);

var viewMatte = ui.checkbox("View Matte", false);


// Render
var origImg = fx.store();

if(invert)
	fx.invertAlpha();

fx.swapChannel(fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_A);
var origMatte = fx.store();
fx.blur(preRadius);

fx.levelsAlpha(in_black, in_white, gamma, out_black, out_white);

fx.bg = origMatte;
bx.blend(blendMode, 100, 100, 100, 100, false);

fx.blur(postRadius);
if(median)
	fx.medianAlpha();
fx.levelsAlpha(postErode, 100, 1.0, 0.0, 100.0);


if(!viewMatte)
{
	fx.bg = origImg;
	fx.swapChannel(fx.CHANNEL_R_BG, fx.CHANNEL_G_BG, fx.CHANNEL_B_BG, fx.CHANNEL_A);
}



