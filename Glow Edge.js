/* Copyright 2012 Red Giant Software. All rights reserved. */
// Plugin Definition
ui.plugin(
		// Fx Name
		"Glow Edge",
		// Category
		"Red Giant Warp",
		// Company Name
		"Red Giant",
		// Company Website
		"http://www.redgiant.com/products/all/rgwarp/",
		// Plugin Description
		"High quality per pixel Glow with Edge processing",
		// Major Version
		"1",
		// Minor Version
		"4",
		// Bug Version
		"0",
		// UUID Fx
		"A65830D2-D27D-4E48-BE07-B2DC52090D35",
		// UUID Category
		"01495B47-8338-4679-95B9-64C6BE7026D0"
);

ui.importModules("lx bx mx gl");

ui.license("discrete");

ui.add_cpp_code("\
#define RG_PRODUCT_LIC_NAME RGS::kTransform\n\
#define SUITE_1_SERIAL_LIB\n\
#define SUITE_1_PRODUCT RGS::kProKeyingSuite\n\
#define SUITE_1_MAJOR 11\n\
#define SUITE_1_MINOR 0\n\
#define SUITE_1_BUG 0\n\
#define SUITE_2_SERIAL_LIB\n\
#define SUITE_2_PRODUCT RGS::kEffectsSuite\n\
#define SUITE_2_MAJOR 10\n\
#define SUITE_2_MINOR 0\n\
#define SUITE_2_BUG 0\n\
");


ui.camera = true;
ui.lights = true;

ui.update = true;
// Properties

ui.begin();
var amount = ui.slider("Edges", 100.0, 0.0, 1000.0);


var brushIndex = ui.menu("Shape", 0, ["Gaussian", "Hoop", "Kuplah", "Mass", "Six Flare", "Six Spike Horizontal", "Six Spike Vertical",
	"Six Spike Whack", "Spectacle", "Tent", "Tree", "Whoopy", "Zig", "Custom Shape"]);
var shapeLayer = ui.layer("Shape Layer");
var radius = ui.slider("Radius", 10, 0, 100);
var intensity = ui.slider("Intensity", 100.0, 0.0, 500.0);
var gamma = ui.slider("Falloff", 1, 0.1, 5.0);
var threshold = ui.slider("Threshold", 90, 0.0, 100.0);

ui.groupBegin("Input");
var glowLayer = ui.layer("Glow Layer");
var blurInput = ui.slider("Blur Input", 0.0, 0.0, 100.0);
var quality = ui.menu("Quality", 2, ["1:1 (Highest)", "2:1", "4:1", "8:1", "16:1 (Lowest)"]);
var inputSource = ui.menu("Input Source Channels", 0, ["RGBA","Lightness","Luma", "Alpha", "Red", "Green", "Blue"]);
ui.groupEnd();


ui.groupBegin("Mask");
var shape1 = ui.menu("Mask Shape", 0, ["Off","Rectangle","Ellipse"]);
var invert1 = ui.checkbox("Mask Invert", false);
var pointA1 = ui.point("Mask Point A", [0.500000, 0.2500000]);
var pointB1 = ui.point("Mask Point B", [0.500000, 0.7500000]);
var radius1 = ui.slider("Mask Radius", 25.000000, 0.010000, 100.000000);
var featherSize1 = ui.slider("Mask Feather Size", 20.00000, 0.000000, 100.000000);
var featherDirection1 = ui.slider("Mask Feather Direction", 0.000000, -100.00000, 100.000000);
ui.groupEnd();

ui.groupBegin("Output");
var prevent = ui.checkbox("Prevent Overbrights", false);
var strength = ui.slider("Overbright Clamp", 100.000000, 0.000000, 100.000000);
var viewMode = ui.menu("View", 0, ["Output", "Brightness Mask", "Mask"]);
var channels = ui.menu("Channels", 1, ["RGB", "RGBA", "Red", "Green", "Blue", "Alpha"]);
var bg_a_mix = ui.slider("Source Opacity", 100.0, 0.0, 100.0);
var fg_a_mix = ui.slider("Glow Opacity", 100.0, 0.0, 100.0);
var blendMode = ui.blendMenu("Blend Mode",  bx.ADD);
ui.groupEnd();
ui.finish();

if(ui.doUpdate())
{
	ui.enable = brushIndex==13;
  	ui.updateProperty("Shape Layer");

	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Invert");
  	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Point A");
  	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Point B");
  	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Radius");
  	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Feather Size");
  	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Feather Direction");

	ui.enable = prevent;
  	ui.updateProperty("Overbright Clamp");

  	return;
}

// Render
var scaleArray = [1.0, 2.0, 4.0, 8.0, 16.0];
var scale = scaleArray[quality];
var min_Threshold = threshold * 0.01;
var max_Threshold = 0.5;
var min_softness = 1.0;
var max_softness = 10.0;

var mask_r = true;
var mask_g = true;
var mask_b = true;
var mask_a = true;
switch(channels)
{
case 0 :
	mask_r = false;
	mask_g = false;
	mask_b = false;
	break;
case 1 :
	mask_r = false;
	mask_g = false;
	mask_b = false;
	mask_a = false;
	break;
case 2 :
	mask_r = false;
	break;
case 3 :
	mask_g = false;
	break;
case 4 :
	mask_b = false;
	break;
case 5 :
	mask_a = false;
	break;
}


var orig = fx.store();

var glowImg = fx.layer(glowLayer, fx.time);
if(fx.isValidImage(glowImg))
{
	fx.blit(glowImg);
}

fx.edges(amount);

fx.blur(blurInput*0.01);
switch(inputSource)
{
        case 0:
            break;
        case 1:
            fx.channelExpr("a*(r+g+b)*0.333333", "a*(r+g+b)*0.333333", "a*(r+g+b)*0.333333", "a*(r+g+b)*0.333333");
            break;
        case 2:
            fx.channelExpr("a*(117.0*r+204.0*g+64.0*b)/385.0", "a*(117.0*r+204.0*g+64.0*b)/385.0", "a*(117.0*r+204.0*g+64.0*b)/385.0","a*(117.0*r+204.0*g+64.0*b)/385.0");
            break;
        case 3:
            break;
        case 4:
            fx.channelExpr("r*a", "r*a", "r*a","r*a");
            break;
        case 5:
            fx.channelExpr("g*a", "g*a", "g*a","g*a");
            break;
        case 6:
            fx.channelExpr("b*a", "b*a", "b*a","b*a");
            break;
}

fx.threshold(min_Threshold, max_Threshold, min_softness, max_softness);
fx.thresholdAlpha(min_Threshold, max_Threshold, min_softness, max_softness);
fx.power(gamma);
fx.powerAlpha(gamma);

if(viewMode == 2 && shape1 == 0)
{
	fx.setRgba(1.0, 1.0, 1.0, 1.0);
	return;
}

if(viewMode == 1)
{
	return;
}

var lumaMap = fx.store();


var brush;
if(brushIndex == 13)
{
	brush = fx.layer(shapeLayer, fx.time, false);
}
else
{
	var brushnames = ["$lib/Brushes/gaussian.brush", "$lib/Brushes/hoop.brush", "$lib/Brushes/kuplah.brush", "$lib/Brushes/mass.brush", "$lib/Brushes/sixFlare.brush", 
	"$lib/Brushes/sixSpikeHoriz.brush", "$lib/Brushes/sixSpikeVert.brush", "$lib/Brushes/sixSpikeWhack.brush", "$lib/Brushes/spectacle.brush", 
	"$lib/Brushes/tent.brush", "$lib/Brushes/tree.brush", "$lib/Brushes/whoopy.brush", "$lib/Brushes/zig.brush", "$lib/Brushes/customGlow shape"];

	brush = fx.brush(brushnames[brushIndex]);
}

// Render
if(viewMode == 0)
{
	gl.begin();
	gl.clear([0.0, 0.0, 0.0], 0.0);
	
	gl.perspectiveTransition(0.5, 0.0);
	gl.disable(gl.DEPTH_TEST);
	gl.light(true, 1.0, 1.0);
	
	gl.imageGeom(brush, lumaMap, scale, radius, intensity*0.01, false, mask_r, mask_g, mask_b, mask_a);
	gl.end();
}

if(prevent)
	lx.autoShoulder(strength);

if(shape1 != 0)
{
	var alpha = true;
	var redOpacity = 1.0;
	var mode = 0;//Apply
	if(viewMode == 2)
	{
		fx.setRgba(1.0, 1.0, 1.0, 1.0);
		mode = 0;
	}
	mx.powerMask(shape1-1,mode,invert1,pointA1,pointB1,radius1,featherSize1,featherDirection1,redOpacity, alpha);
	if(viewMode == 2)
		return;
}

fx.bg = orig;
bx.blend(blendMode, 100, 100, fg_a_mix, bg_a_mix, false);


