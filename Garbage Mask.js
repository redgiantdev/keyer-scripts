/* Copyright 2012 Red Giant Software. All rights reserved. */
// Plugin Definition
ui.plugin(
		// Fx Name
		"Garbage Mask",
		// Category
		"Primatte",
		// Company Name
		"Red Giant",
		// Company Website
		"http://www.redgiant.com/products/all/keying-suite",
		// Plugin Description
		"Garbage mask combiner plugin of up to 4 masks",
		// Major Version
		"1",
		// Minor Version
		"0",
		// Bug Version
		"0",
		// UUID Fx
		"905B3E8A-237F-4FA2-B667-81DC72CA6D48",
		// UUID Category
		"01495B47-8338-4679-95B9-64C6BE7026D0"
);

ui.importModules("bx mx");

ui.license("discrete");

ui.add_cpp_code("\
#define RG_PRODUCT_LIC_NAME RGS::kPrimatteKeyer\n\
#define SUITE_1_SERIAL_LIB\n\
#define SUITE_1_PRODUCT RGS::kProKeyingSuite\n\
#define SUITE_1_MAJOR 11\n\
#define SUITE_1_MINOR 0\n\
#define SUITE_1_BUG 0\n\
");

// Properties
var redOpacity = 0.5;
var mode = 1;
var alpha = true;

ui.groupBegin("Mask 1");
var shape1 = ui.menu("Shape", 1, ["Off","Rectangle","Ellipse"]);
var invert1 = ui.checkbox("Invert", false);
var pointA1 = ui.point("Point A", [0.500000, 0.2500000]);
var pointB1 = ui.point("Point B", [0.500000, 0.7500000]);
var radius1 = ui.slider("Radius", 25.000000, 0.010000, 100.000000);
var featherSize1 = ui.slider("Feather Size", 0.00000, 0.000000, 100.000000);
var featherDirection1 = ui.slider("Feather Direction", 0.000000, -100.00000, 100.000000);
var blendMode1 = ui.blendMenu("Blend Mode",  bx.NONE);
ui.groupEnd();

ui.groupBegin("Mask 2");
var shape2 = ui.menu("Shape", 0, ["Off","Rectangle","Ellipse"]);
var invert2 = ui.checkbox("Invert", true);
var pointA2 = ui.point("Point A", [0.7500000, 0.100000]);
var pointB2 = ui.point("Point B", [0.7500000, 0.400000]);
var radius2 = ui.slider("Radius", 25.000000, 0.010000, 100.000000);
var featherSize2 = ui.slider("Feather Size", 0.00000, 0.000000, 100.000000);
var featherDirection2 = ui.slider("Feather Direction", 0.000000, -100.00000, 100.000000);
var blendMode2 = ui.blendMenu("Blend Mode",  bx.NONE);
ui.groupEnd();

ui.groupBegin("Mask 3");
var shape3 = ui.menu("Shape", 0, ["Off","Rectangle","Ellipse"]);
var invert3 = ui.checkbox("Invert", true);
var pointA3 = ui.point("Point A", [0.7500000, 0.600000]);
var pointB3 = ui.point("Point B", [0.7500000, 0.900000]);
var radius3 = ui.slider("Radius", 25.000000, 0.010000, 100.000000);
var featherSize3 = ui.slider("Feather Size", 0.00000, 0.000000, 100.000000);
var featherDirection3 = ui.slider("Feather Direction", 0.000000, -100.00000, 100.000000);
var blendMode3 = ui.blendMenu("Blend Mode",  bx.NONE);
ui.groupEnd();

ui.groupBegin("Mask 4");
var shape4 = ui.menu("Shape", 0, ["Off","Rectangle","Ellipse"]);
var invert4 = ui.checkbox("Invert", true);
var pointA4 = ui.point("Point A", [0.2500000, 0.600000]);
var pointB4 = ui.point("Point B", [0.2500000, 0.900000]);
var radius4 = ui.slider("Radius", 25.000000, 0.010000, 100.000000);
var featherSize4 = ui.slider("Feather Size", 0.00000, 0.000000, 100.000000);
var featherDirection4 = ui.slider("Feather Direction", 0.000000, -100.00000, 100.000000);
var blendMode4 = ui.blendMenu("Blend Mode",  bx.NONE);
ui.groupEnd();


var postRadius = ui.slider("Post Blur", 0.0, 0.0, 100.0);
var postErode = ui.slider("Post Erode", 0.0, 0.0, 100.0);
var median = ui.checkbox("Post Median", false);

var viewMatte = ui.checkbox("View Matte", false);


// Render
var origImg = fx.store();

fx.swapChannel(fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_A);
var origMatte = fx.store();

if(shape1 != 0)
{
	mx.powerMask(shape1-1,mode,invert1,pointA1,pointB1,radius1,featherSize1,featherDirection1,redOpacity, alpha);
	fx.swapChannel(fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_R);

	fx.bg = origMatte;
	bx.blend(blendMode1, 100, 100, 100, 100, false);
	var origMatte = fx.store();
}

if(shape2 != 0)
{
	mx.powerMask(shape2-1,mode,invert2,pointA2,pointB2,radius2,featherSize2,featherDirection2,redOpacity, alpha);
	fx.swapChannel(fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_R);

	fx.bg = origMatte;
	bx.blend(blendMode2, 100, 100, 100, 100, false);
	var origMatte = fx.store();
}

if(shape3 != 0)
{
	mx.powerMask(shape3-1,mode,invert3,pointA3,pointB3,radius3,featherSize3,featherDirection3,redOpacity, alpha);
	fx.swapChannel(fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_R);

	fx.bg = origMatte;
	bx.blend(blendMode3, 100, 100, 100, 100, false);
	var origMatte = fx.store();
}

if(shape4 != 0)
{
	mx.powerMask(shape4-1,mode,invert4,pointA4,pointB4,radius4,featherSize4,featherDirection4,redOpacity, alpha);
	fx.swapChannel(fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_R);

	fx.bg = origMatte;
	bx.blend(blendMode4, 100, 100, 100, 100, false);
	var origMatte = fx.store();
}

fx.blur(postRadius);
if(median)
	fx.medianAlpha();
fx.levelsAlpha(postErode, 100, 1.0, 0.0, 100.0);


if(!viewMatte)
{
	fx.bg = origImg;
	fx.swapChannel(fx.CHANNEL_R_BG, fx.CHANNEL_G_BG, fx.CHANNEL_B_BG, fx.CHANNEL_A);
}



