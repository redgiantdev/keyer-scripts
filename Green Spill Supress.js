/* Copyright 2012 Red Giant Software. All rights reserved. */
// Plugin Definition
ui.plugin(
		// Fx Name
		"Green Spill Supress",
		// Category
		"Primatte",
		// Company Name
		"Red Giant",
		// Company Website
		"http://www.redgiant.com/products/all/keying-suite",
		// Plugin Description
		"Green Screen Spill Supression",
		// Major Version
		"1",
		// Minor Version
		"0",
		// Bug Version
		"0",
		// UUID Fx
		"3B2E94A2-F5E1-438D-BCFA-A1ED7ABB3305",
		// UUID Category
		"01495B47-8338-4679-95B9-64C6BE7026D0"
);

ui.importModules("lx bx mx");

ui.update = true;

ui.license("discrete");

ui.add_cpp_code("\
#define RG_PRODUCT_LIC_NAME RGS::kPrimatteKeyer\n\
#define SUITE_1_SERIAL_LIB\n\
#define SUITE_1_PRODUCT RGS::kProKeyingSuite\n\
#define SUITE_1_MAJOR 11\n\
#define SUITE_1_MINOR 0\n\
#define SUITE_1_BUG 0\n\
");


// Properties

var hue = ui.slider("Hue", -100, -100, 100);
var sat = ui.slider("Saturation", 0.0, -100, 100);
var luma = ui.slider("Luma", 0.0, -100, 100);

ui.groupBegin("Mask");
var shape1 = ui.menu("Mask Shape", 0, ["Off","Rectangle","Ellipse"]);
var invert1 = ui.checkbox("Mask Invert", false);
var pointA1 = ui.point("Mask Point A", [0.500000, 0.2500000]);
var pointB1 = ui.point("Mask Point B", [0.500000, 0.7500000]);
var radius1 = ui.slider("Mask Radius", 25.000000, 0.010000, 100.000000);
var featherSize1 = ui.slider("Mask Feather Size", 20.00000, 0.000000, 100.000000);
var featherDirection1 = ui.slider("Mask Feather Direction", 0.000000, -100.00000, 100.000000);
ui.groupEnd();

var opacity = ui.slider("Supression Amount", 100.0, 0, 100);
var blendMode = ui.blendMenu("Blend Mode",  bx.NORMAL);

if(ui.doUpdate())
{
	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Invert");
  	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Point A");
  	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Point B");
  	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Radius");
  	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Feather Size");
  	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Feather Direction");

  	return;
}


// Render
var orig = fx.store();


// Render
lx.rangedHSL(0.0, 0.0, 0.0, hue, 0.0, 0.0, 0.0, 0.0,    	//  Hue (+/-100%): R, O, Y, G, C, B, P, M
		  0.0, 0.0, 0.0, sat, 0.0, 0.0, 0.0, 0.0,   		//  Sat (+/-100%): R, O, Y, G, C, B, P, M
		  0.0, 0.0, 0.0, luma, 0.0, 0.0, 0.0, 0.0		   	//  Lum (+/-100%): R, O, Y, G, C, B, P, M
		  );

if(shape1 != 0)
{
	var alpha = 1.0;
	var redOpacity = 1.0;
	var mode = 0;//Apply
	mx.powerMask(shape1-1,mode,invert1,pointA1,pointB1,radius1,featherSize1,featherDirection1,redOpacity, alpha);
}

fx.bg = orig;
bx.blend(blendMode, 100, 100, opacity, 100.0, false);



