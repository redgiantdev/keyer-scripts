/* Copyright 2012 Red Giant Software. All rights reserved. */
// Plugin Definition
ui.plugin(
		// Fx Name
		"Glow Lite",
		// Category
		"Red Giant Warp",
		// Company Name
		"Red Giant",
		// Company Website
		"http://www.redgiant.com/products/all/rgwarp/",
		// Plugin Description
		"High quality per pixel Glow with simplified interface",
		// Major Version
		"1",
		// Minor Version
		"4",
		// Bug Version
		"0",
		// UUID Fx
		"B0CD9E78-BBD7-40B4-9B01-A4B2A39FEE0C",
		// UUID Category
		"01495B47-8338-4679-95B9-64C6BE7026D0"
);

ui.importModules("lx bx mx gl");

ui.license("discrete");

ui.add_cpp_code("\
#define RG_PRODUCT_LIC_NAME RGS::kTransform\n\
#define SUITE_1_SERIAL_LIB\n\
#define SUITE_1_PRODUCT RGS::kProKeyingSuite\n\
#define SUITE_1_MAJOR 11\n\
#define SUITE_1_MINOR 0\n\
#define SUITE_1_BUG 0\n\
#define SUITE_2_SERIAL_LIB\n\
#define SUITE_2_PRODUCT RGS::kEffectsSuite\n\
#define SUITE_2_MAJOR 10\n\
#define SUITE_2_MINOR 0\n\
#define SUITE_2_BUG 0\n\
");


// Properties
var brushIndex = ui.menu("Shape", 0, ["Gaussian", "Hoop", "Kuplah", "Mass", "Six Flare", "Six Spike Horizontal", "Six Spike Vertical",
	"Six Spike Whack", "Spectacle", "Tent", "Tree", "Whoopy", "Zig"]);
var radius = ui.slider("Radius", 10, 0, 100);
var intensity = ui.slider("Intensity", 100.0, 0.0, 500.0);
var gamma = ui.slider("Falloff", 1, 0.1, 5.0);
var threshold = ui.slider("Threshold", 90, 0.0, 100.0);
var quality = ui.menu("Quality", 2, ["1:1 (Highest)", "2:1", "4:1", "8:1", "16:1 (Lowest)"]);


ui.groupBegin("Output");
var prevent = ui.checkbox("Prevent Overbrights", false);
var strength = 100.0;
var viewMode = ui.menu("View", 0, ["Output", "Brightness Mask", "Mask"]);
var channels = ui.menu("Channels", 1, ["RGB", "RGBA", "Red", "Green", "Blue", "Alpha"]);
var bg_a_mix = ui.slider("Source Opacity", 100.0, 0.0, 100.0);
var fg_a_mix = ui.slider("Glow Opacity", 100.0, 0.0, 100.0);
var blendMode = ui.blendMenu("Blend Mode",  bx.ADD);
ui.groupEnd();


// Render
var scaleArray = [1.0, 2.0, 4.0, 8.0, 16.0];
var scale = scaleArray[quality];

var min_Threshold = threshold * 0.01;
var max_Threshold = 0.5;
var min_softness = 1.0;
var max_softness = 10.0;

var mask_r = true;
var mask_g = true;
var mask_b = true;
var mask_a = true;
switch(channels)
{
case 0 :
	mask_r = false;
	mask_g = false;
	mask_b = false;
	break;
case 1 :
	mask_r = false;
	mask_g = false;
	mask_b = false;
	mask_a = false;
	break;
case 2 :
	mask_r = false;
	break;
case 3 :
	mask_g = false;
	break;
case 4 :
	mask_b = false;
	break;
case 5 :
	mask_a = false;
	break;
}


var orig = fx.store();

fx.threshold(min_Threshold, max_Threshold, min_softness, max_softness);
fx.thresholdAlpha(min_Threshold, max_Threshold, min_softness, max_softness);
fx.power(gamma);
fx.powerAlpha(gamma);


var lumaMap = fx.store();


var brush;
var brushnames = ["$lib/Brushes/gaussian.brush", "$lib/Brushes/hoop.brush", "$lib/Brushes/kuplah.brush", "$lib/Brushes/mass.brush", "$lib/Brushes/sixFlare.brush", 
	"$lib/Brushes/sixSpikeHoriz.brush", "$lib/Brushes/sixSpikeVert.brush", "$lib/Brushes/sixSpikeWhack.brush", "$lib/Brushes/spectacle.brush", 
	"$lib/Brushes/tent.brush", "$lib/Brushes/tree.brush", "$lib/Brushes/whoopy.brush", "$lib/Brushes/zig.brush", "$lib/Brushes/customGlow shape"];

brush = fx.brush(brushnames[brushIndex]);

gl.begin();
gl.clear([0.0, 0.0, 0.0], 0.0);

gl.perspectiveTransition(0.5, 0.0);
gl.disable(gl.DEPTH_TEST);

gl.imageGeom(brush, lumaMap, scale, radius, intensity*0.01, false, mask_r, mask_g, mask_b, mask_a);
gl.end();

// Render

if(prevent)
	lx.autoShoulder(100.0);


fx.bg = orig;
bx.blend(blendMode, 100, 100, fg_a_mix, bg_a_mix, false);


