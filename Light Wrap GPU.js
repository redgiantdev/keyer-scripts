/* Copyright 2012 Red Giant Software. All rights reserved. */
// Plugin Definition
ui.plugin(
		// Fx Name
		"Light Wrap GPU",
		// Category
		"Primatte",
		// Company Name
		"Red Giant",
		// Company Website
		"http://www.redgiant.com/products/all/keying-suite",
		// Plugin Description
		"Full featured Keying Supression",
		// Major Version
		"1",
		// Minor Version
		"0",
		// Bug Version
		"0",
		// UUID Fx
		"DB14EC9A-2976-4CA9-84CF-512DA7E83F58",
		// UUID Category
		"01495B47-8338-4679-95B9-64C6BE7026D0"
);

ui.importModules("lx mx m2 tx");

ui.update = true;

ui.license("discrete");

ui.add_cpp_code("\
#define RG_PRODUCT_LIC_NAME RGS::kPrimatteKeyer\n\
#define SUITE_1_SERIAL_LIB\n\
#define SUITE_1_PRODUCT RGS::kProKeyingSuite\n\
#define SUITE_1_MAJOR 11\n\
#define SUITE_1_MINOR 0\n\
#define SUITE_1_BUG 0\n\
");



// Properties
var bg_layer = ui.layer("Background");
var lw_width = ui.slider("Width", 4.00, 0.00, 200.00);
var lw_bg_blur = ui.slider("Background Blur", 6.00, 0.00, 100.00);
var lw_brightness = ui.slider("Brightness", 0.00, -1.00, 1.00);
var view_mode = ui.checkbox("Show LightWrap", false);

var bg_img = fx.layer(bg_layer, fx.time);


var key_img = fx.store();

if(!fx.isValidImage(bg_img))
{
	fx.setRgba(0.0, 0.0, 0.0, 1 /*Alpha*/);	
	bg_img = fx.store();
}
else
	fx.blit(bg_img);

// Blit and Blur BG and apply brightness exposure
fx.blur(lw_bg_blur);
fx.exposure(lw_brightness);
// Combine the alpha from the keyed image (multiply it with the BG's alpha)
fx.bg = key_img;
bx.blendExpr("fg.r", "fg.g", "fg.b", "fg.a*bg.a");
// Blur and invert the alpha
fx.blurAlpha(lw_width);
fx.invertAlpha();
// Stow a copy if we need it in the view/split screen
if(view_mode)
{
    return;
}
// Do an infront of the light wrap over the keyed image
fx.bg = key_img;
bx.infront(100 /*FG RGB Mix*/, 100 /*BG RGB Mix*/, 100 /*FG Alpha Mix*/, 100 /*BG Alpha Mix*/, false);
// Keep original keyed alpha
fx.bg = key_img;
fx.swapChannel(fx.CHANNEL_R , fx.CHANNEL_G , fx.CHANNEL_B , fx.CHANNEL_A_BG);


        