/* Copyright 2012 Red Giant Software. All rights reserved. */
// Plugin Definition
ui.plugin(
		// Fx Name
		"Matte Combine",
		// Category
		"Primatte",
		// Company Name
		"Red Giant",
		// Company Website
		"http://www.redgiant.com/products/all/keying-suite",
		// Plugin Description
		"Matte multi composite combiner plugin of up to 4 layers",
		// Major Version
		"1",
		// Minor Version
		"0",
		// Bug Version
		"0",
		// UUID Fx
		"91AEA014-E433-455F-9641-5BB72D7CB9AC",
		// UUID Category
		"01495B47-8338-4679-95B9-64C6BE7026D0"
);
ui.importModules("bx");

ui.license("discrete");

ui.add_cpp_code("\
#define RG_PRODUCT_LIC_NAME RGS::kPrimatteKeyer\n\
#define SUITE_1_SERIAL_LIB\n\
#define SUITE_1_PRODUCT RGS::kProKeyingSuite\n\
#define SUITE_1_MAJOR 11\n\
#define SUITE_1_MINOR 0\n\
#define SUITE_1_BUG 0\n\
");


// Properties

ui.groupBegin("Layer 1");
var layer1 = ui.layer("Layer");
var luma1 = ui.checkbox("Use Luma", false);
var blur1 = ui.slider("Blur", 0.0, 0.0, 100.0);
var erode1 = ui.slider("Erode", 0, -1000.0, 100.0);
var blendMode1 = ui.blendMenu("Blend Mode",  bx.NORMAL);
ui.groupEnd();

ui.groupBegin("Layer 2");
var layer2 = ui.layer("Layer");
var luma2 = ui.checkbox("Use Luma", false);
var blur2 = ui.slider("Blur", 0.0, 0.0, 100.0);
var erode2 = ui.slider("Erode", 0, -1000.0, 100.0);
var blendMode2 = ui.blendMenu("Blend Mode",  bx.NORMAL);
ui.groupEnd();

ui.groupBegin("Layer 3");
var layer3 = ui.layer("Layer");
var luma3 = ui.checkbox("Use Luma", false);
var blur3 = ui.slider("Blur", 0.0, 0.0, 100.0);
var erode3 = ui.slider("Erode", 0, -1000.0, 100.0);
var blendMode3 = ui.blendMenu("Blend Mode",  bx.NORMAL);
ui.groupEnd();

ui.groupBegin("Layer 4");
var layer4 = ui.layer("Layer");
var luma4 = ui.checkbox("Use Luma", false);
var blur4 = ui.slider("Blur", 0.0, 0.0, 100.0);
var erode4 = ui.slider("Erode", 0, -1000.0, 100.0);
var blendMode4 = ui.blendMenu("Blend Mode",  bx.NORMAL);
ui.groupEnd();


var postRadius = ui.slider("Post Blur", 0.0, 0.0, 100.0);
var postErode = ui.slider("Post Erode", 0.0, 0.0, 100.0);
var median = ui.checkbox("Post Median", false);

var viewMatte = ui.checkbox("View Matte", false);


// Render
var origImg = fx.store();

fx.swapChannel(fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_A);
var origMatte = fx.store();

var img;
img = fx.layer(layer1, fx.time);
if(fx.isValidImage(img))
{
	fx.blit(img);
	if(luma1)
		fx.swapChannel(fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_L);
	else
		fx.swapChannel(fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_A);

	fx.blur(blur1);
	fx.levelsAlpha(erode1>=100.0?99.0:erode1, 100.0, 1.0, 0.0, 100.0);
	fx.bg = origMatte;
	bx.blend(blendMode1, 100, 100, 100, 100, false);
	var origMatte = fx.store();
}
img = fx.layer(layer2, fx.time);
if(fx.isValidImage(img))
{
	fx.blit(img);
	if(luma2)
		fx.swapChannel(fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_L);
	else
		fx.swapChannel(fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_A);
	fx.blur(blur2);
	fx.levelsAlpha(erode2>=100.0?99.0:erode2, 100.0, 1.0, 0.0, 100.0);
	fx.bg = origMatte;
	bx.blend(blendMode2, 100, 100, 100, 100, false);
	var origMatte = fx.store();
}
img = fx.layer(layer3, fx.time);
if(fx.isValidImage(img))
{
	fx.blit(img);
	fx.blur(blur3);
	if(luma3)
		fx.swapChannel(fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_L);
	else
		fx.swapChannel(fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_A);
	fx.levelsAlpha(erode3>=100.0?99.0:erode3, 100.0, 1.0, 0.0, 100.0);
	fx.bg = origMatte;
	bx.blend(blendMode3, 100, 100, 100, 100, false);
	var origMatte = fx.store();
}
img = fx.layer(layer4, fx.time);
if(fx.isValidImage(img))
{
	fx.blit(img);
	fx.blur(blur4);
	if(luma4)
		fx.swapChannel(fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_L);
	else
		fx.swapChannel(fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_ONE, fx.CHANNEL_A);
	fx.levelsAlpha(erode4>=100.0?99.0:erode4, 100.0, 1.0, 0.0, 100.0);
	fx.bg = origMatte;
	bx.blend(blendMode4, 100, 100, 100, 100, false);
	var origMatte = fx.store();
}

fx.blur(postRadius);
if(median)
	fx.medianAlpha();
fx.levelsAlpha(postErode, 100, 1.0, 0.0, 100.0);


if(!viewMatte)
{
	fx.bg = origImg;
	fx.swapChannel(fx.CHANNEL_R_BG, fx.CHANNEL_G_BG, fx.CHANNEL_B_BG, fx.CHANNEL_A);
}



