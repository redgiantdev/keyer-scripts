/* Copyright 2012 Red Giant Software. All rights reserved. */
// Plugin Definition
ui.plugin(
		// Fx Name
		"Spill Supress",
		// Category
		"Primatte",
		// Company Name
		"Red Giant",
		// Company Website
		"http://www.redgiant.com/products/all/keying-suite",
		// Plugin Description
		"Full featured Keying Supression",
		// Major Version
		"1",
		// Minor Version
		"0",
		// Bug Version
		"0",
		// UUID Fx
		"9838026D-A020-4491-9886-700F774867C6",
		// UUID Category
		"01495B47-8338-4679-95B9-64C6BE7026D0"
);

ui.importModules("lx mx m2 tx");

ui.update = true;

ui.license("discrete");

ui.add_cpp_code("\
#define RG_PRODUCT_LIC_NAME RGS::kPrimatteKeyer\n\
#define SUITE_1_SERIAL_LIB\n\
#define SUITE_1_PRODUCT RGS::kProKeyingSuite\n\
#define SUITE_1_MAJOR 11\n\
#define SUITE_1_MINOR 0\n\
#define SUITE_1_BUG 0\n\
");


// Properties
var rgb_spill = ui.color("Spill", [97.0/255.0, 204.0/255.0, 242.0/255.0]);

var rgb_replace = ui.color("Replace", [255.0/255.0, 126.0/255.0, 143/255.0]);

ui.groupBegin("Replace Layer");
var layer_replace = ui.layer("Layer");
var replace_blur = ui.slider("Blur", 5.0, 0, 100);
var replace_dissolve = ui.slider("Dissolve", 50.0, 0, 100);
ui.groupEnd();

ui.groupBegin("Hue");
var hD = ui.checkbox("Hue Disable", false);
var hNR = ui.slider("Hue Neg Range", 5.0, 0, 100);
var hPR = ui.slider("Hue Pos Range", 5.0, 0, 100);
var hNS = ui.slider("Hue Neg Soft", 20.0, 0, 100);
var hPS = ui.slider("Hue Pos Soft", 20.0, 0, 100);
ui.groupEnd();

ui.groupBegin("Sat");
var sD = ui.checkbox("Sat Disable", false);
var sNR = ui.slider("Sat Neg Range", 30.0, 0, 100);
var sPR = ui.slider("Sat Pos Range", 30.0, 0, 100);
var sNS = ui.slider("Sat Neg Soft", 20.0, 0, 100);
var sPS = ui.slider("Sat Pos Soft", 20.0, 0, 100);
ui.groupEnd();

ui.groupBegin("Luma");
var lD = ui.checkbox("Luma Disable", false);
var lNR = ui.slider("Luma Neg Range", 30.0, 0, 100);
var lPR = ui.slider("Luma Pos Range", 30.0, 0, 100);
var lNS = ui.slider("Luma Neg Soft", 20.0, 0, 100);
var lPS = ui.slider("Luma Pos Soft", 20.0, 0, 100);
ui.groupEnd();


var bc = ui.slider("Black Clip", 0.0, 0, 100);
var wc = ui.slider("White Clip", 100.0, 0, 100);
var blur = ui.slider("Blur", 0.0, 0, 100);
var invert = ui.checkbox("Invert", false);

ui.groupBegin("Mask");
var shape1 = ui.menu("Mask Shape", 0, ["Off","Rectangle","Ellipse"]);
var invert1 = ui.checkbox("Mask Invert", false);
var pointA1 = ui.point("Mask Point A", [0.500000, 0.2500000]);
var pointB1 = ui.point("Mask Point B", [0.500000, 0.7500000]);
var radius1 = ui.slider("Mask Radius", 25.000000, 0.010000, 100.000000);
var featherSize1 = ui.slider("Mask Feather Size", 20.00000, 0.000000, 100.000000);
var featherDirection1 = ui.slider("Mask Feather Direction", 0.000000, -100.00000, 100.000000);
ui.groupEnd();

var viewSpill = ui.checkbox("View Spill", false);

// Enable/Disable - HSL, PowerMsk, ReplaceLyr, 
if(ui.doUpdate())
{
	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Invert");
  	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Point A");
  	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Point B");
  	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Radius");
  	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Feather Size");
  	ui.enable = shape1!=0;
  	ui.updateProperty("Mask Feather Direction");

 	ui.enable = !hD;
	ui.updateProperty("Hue Neg Range");
	ui.enable = !hD;
	ui.updateProperty("Hue Pos Range");
	ui.enable = !hD;
	ui.updateProperty("Hue Neg Soft");
	ui.enable = !hD;
	ui.updateProperty("Hue Pos Soft");

	ui.enable = !sD;
	ui.updateProperty("Sat Neg Range");
	ui.enable = !sD;
	ui.updateProperty("Sat Pos Range");
	ui.enable = !sD;
	ui.updateProperty("Sat Neg Soft");
	ui.enable = !sD;
	ui.updateProperty("Sat Pos Soft");

	ui.enable = !lD;
	ui.updateProperty("Luma Neg Range");
	ui.enable = !lD;
	ui.updateProperty("Luma Pos Range");
	ui.enable = !lD;
	ui.updateProperty("Luma Neg Soft");
	ui.enable = !lD;
	ui.updateProperty("Luma Pos Soft");


  	return;
}


// Render
var orig = fx.store();

var hsl_spill = m2.rgbToHsl(rgb_spill);
var hsl_replace = m2.rgbToHsl(rgb_replace);
var hC = hsl_spill[0]*100.0;
var sC = hsl_spill[1]*100.0;
var lC = hsl_spill[2]*100.0;
var showKey = false;

lx.keyer(hD, hC, hNR, hPR, hNS, hPS, 
		sD, sC, sNR, sPR, sNS, sPS,
		lD, lC, lNR, lPR, lNS, lPS,
		bc, wc, blur, invert, showKey);

if(shape1 != 0)
{
	var mask = fx.store();
	var alpha = 1.0;
	var redOpacity = 1.0;
	var mode = 0;//Apply
	mx.powerMask(shape1-1,mode,invert1,pointA1,pointB1,radius1,featherSize1,featherDirection1,redOpacity, alpha);
}

if(viewSpill)
{
	fx.setRgb(1.0, 1.0, 1.0);
	return;
}

fx.invertAlpha();

var mask = fx.store();

fx.blit(orig);
fx.setRgb(rgb_replace[0], rgb_replace[1], rgb_replace[2]);
var replace_img = fx.layer(layer_replace, fx.time);
if(fx.isValidImage(replace_img))
{
	fx.bg = replace_img;
	tx.dissolve(replace_dissolve);
	fx.blur(replace_blur);
}


fx.bg = orig;
fx.mask = mask;

tx.maskAlpha(100.0, 100.0);


